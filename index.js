const express = require('express')
const app = express()
const request = require('superagent')
const bodyParser = require('body-parser')
const _ = require('lodash')

// Capital One creds
const CapitalOne = {
  API_URL: "https://api.devexhacks.com",
  CLIENT_ID: "vgw3sf4f8nq3b98i1gdfr8wpx4gpty0ska52",
  SECRET: "eb5f6rda6v0d1ld8y4fymkudo86gorrc47cj",
  USERNAME: "hackathon-user",
  PASSWORD: "Hackathon@123",

  // Hard-coded Oauth
  AUTH_CODE: "8151af36a0a8478987117a960308bfb1",
  ACCESS_KEY: "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwicGNrIjoxLCJhbGciOiJkaXIiLCJ0diI6Miwia2lkIjoiYTdxIn0..Q8EPUTo189PyagVaeXKw9XgvYN1pEz5Vgp1bgF4Hj9TE2anFkmGILcf7UX9iO6L0cUTgJQm3blatkUZUyUKc6cHFyyuVPKmtZDIU2zmP6VEhxmroUfeqh8YJnOEw9LRVKU1Pq4fVRuZMsIM1Mf6F2oMOAFL8JTw7AK4CQVUWtti4KHaNBtDX9cHOuwRtDbKhQbmySLP0g5ENzrC9gWMLprmq66hX5bI4TAiF2f7KlgjtT9lvph9pLyDsfBhtOanWj6gVmYMqxcNQlUHcgtsH3nlthX1PsOKQppDtmS09hPELzTxEn2kxk2btJ0KPy2iQFQyDSWfER1xgJnFDASr1sg8MNeQh3Qjmp4vuruQMimu1IFVvb1cIsIDS7cWPCUPa2UFYz9YfW1uXVnUpOyZTCWZ3E28YL70Rn2TbP4Hw030rgBWF5Ok1YD51e7BWJXXCq1lIWUG85WmjWZ5Il4nVNZBxBFDPR7lQMG2Gw36ibffzfTDwwHfWhlpkmbqtRLawKEVtYNDcpIvocujQJFHlwCRJ9uex5BXJzQQ6Mrp1cvxp3sp65mU5EPSU4J1OK0Iuj8Yv.I3YRuEIDtnqtHjjjrb9OK0A",
}

app.set('port', (process.env.PORT || 5000))

app.use(bodyParser.json()) // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })) // support encoded bodies
app.use(express.static(__dirname + '/public'))

// views is directory for all template files
app.set('views', __dirname + '/views')
app.set('view engine', 'ejs')

app.get('/', function(req, res) {
  // request
  //   .get('http://google.com/img.png')
  //   .on('response', function(response) {
  //     console.log(response.statusCode) // 200
  //     console.log(response.headers['content-type']) // 'image/png'
  //   })
  //   .pipe(request.put('http://mysite.com/img.png'))
  //   .pipe(response.send)
  res.render('pages/index');
})

app.post('/card', function(req, res) {

  var pan = req.body.pan;
  findCard(pan, function(card) {
    if (!card) {
      res.send("Error getting card")
    } else {
      var cardID = card['rewardsAccountReferenceId']

      groupRewards(cardID, function(rewardGroups) {
        res.send(rewardGroups)
      })
    }
  })

})

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
})

// Returns a card to callback
// example
// {
//     "rewardsAccountReferenceId": "+jaR3Du6APE+x4kQue7NB2l3cLJHm0Rg9qspJbY65DpNtAOoLJnAguw4SVcuOlJuWVrEIiLswYp4ZZ0NX1veFw==",
//     "accountDisplayName": "Capital One Visaplatinum Miles *3582",
//     "rewardsCurrency": "Miles",
//     "productAccountType": "Credit Card",
//     "creditCardAccount": {
//         "issuer": "Capital One",
//         "product": "Visaplatinum",
//         "lastFour": "3582",
//         "network": "Visa",
//         "isBusinessAccount": false
//     }
// }
function findCard(cardPAN, callback) {
  // TODO check network
  var lastFour = cardPAN.substring(cardPAN.length - 4)

  capitalOneApiCall("/rewards/accounts/", function(err, res) {
    if (err) {
      console.error(err)
      callback(null, err)
      return
    }
    var accounts = res['rewardsAccounts']
    // console.log(accounts)

    var account = accounts.filter(function(account) {
      return account['creditCardAccount']['lastFour'] == lastFour
    })[0]

    console.log(account)

    callback(account)
  })
}

// Returns list of rewards, e.g.
// {
// "category": "Travel",
// "categoryDescription": "Travel",
// "subCategory": "Travel.Erase",
// "subcategoryDescription": "Travel Reimbursement",
// "displaySequenceNumber": 1.1,
// "tierMinCashValue": 0.01,
// "tierMaxCashValue": 150,
// "minRedemptionAmount": 15000,
// "redemptionAmount": 15000,
// "cashValue": 150,
// "cashDisplayValue": "$150.00"
// }
function groupRewards(cardID, callback) {
  var url = "/rewards/accounts/" + cardID
  capitalOneApiCall(url, function(err, res) {
    if (err) {
      console.error(err)
      callback([], err)
      return
    }
    var rewards = res['redemptionOpportunities']
    
    var groups = _.groupBy(rewards, 'category')

    callback(groups)
  })
}

function capitalOneApiCall(path, callback) {
  request
    .get(CapitalOne.API_URL + path)
    .set('Authorization', 'Bearer ' + CapitalOne.ACCESS_KEY)
    .set('Accept', 'application/json; v=1')
    .set('Accept-Language', 'en-US')
    .end(function(err, res) {
      var parsed = res && res.body
      callback(err, parsed)
    })
}
